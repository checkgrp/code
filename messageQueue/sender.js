// const amqp = require('amqplib/callback_api');

// const sendToQueue = (input) => {
//     // Step 1: Create Connection
//     amqp.connect('amqp://localhost', (connError, connection) => {
//         if (connError) {
//             throw connError;
//         }
//         // Step 2: Create Channel
//         connection.createChannel((channelError, channel) => {
//             if (channelError) {
//                 throw channelError;
//             }
//             // Step 3: Assert Queue
//             const QUEUE = 'erpqueue'
//             channel.assertQueue(QUEUE);
//             // Step 4: Send message to queue

//             channel.sendToQueue(QUEUE, Buffer.from(input));
//             console.log(`Inserted in queue -> ${QUEUE}`);
//         })
//     })
// }
// module.exports = sendToQueue




var amqp = require('amqplib/callback_api');
const sendToQueue = (tablename,input) => {
amqp.connect('amqp://localhost', function(error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function(error1, channel) {
    if (error1) {
      throw error1;
    }
    console.log(typeof(tablename))
    var exchange = 'erpqueue';
    var key = (tablename) ? tablename : 'anonymous_master';
    var msg = input? input: 'anonymous message';

    channel.assertExchange(exchange, 'topic', {
      durable: false
    });
    channel.publish(exchange, key, Buffer.from(msg));
    console.log(" [x] Sent %s:'%s'", key, msg);
  });

 
});
}
module.exports = sendToQueue
