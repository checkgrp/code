var express = require('express');
var app = express();
var PORT = 6001
const cors = require('cors')

var indexRouter = require('./routes/index');
// var OutboundRoute = require('./routes/outbound/index')
var { SMERP } = require('./config/database');
require('dotenv').config()

var jsonwebtoken = require("jsonwebtoken");


app.use(cors())

app.use(function (req, res, next) {

    var anonymousUrls = ['/master/login'];
    var isAnonymousUrl = anonymousUrls.some(function (regex) {
      var buf = Buffer.from(req.originalUrl);
      return buf.indexOf(regex) > -1
    });
    if (isAnonymousUrl){ return next();
    }
  else{
      const token = req.headers['authorization'];

    if (!token) {
      return res.status(403).send("A token is required for authentication");
    }
    try {
      const decoded = jsonwebtoken.verify(token, process.env.secretKey);
      req.user = decoded;
      next();
    } catch (err) {
      return res.status(401).send("Invalid Token");
      
    }
  }
    
    });




app.use(express.json());
app.use('/master', indexRouter);
// app.use('/outbound', OutboundRoute);


app.listen(PORT, () => console.log(`Server is running on ${PORT}`))