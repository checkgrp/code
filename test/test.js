const { assert, expect } = require('chai');
var chai = require('chai')
    , chaiHttp = require('chai-http');
// var request = require('request');
const group = require('../models/SMERP/group')
const axios = require('axios')
let should = chai.should();
chai.use(chaiHttp)

describe('POST API', () => {
    it('create group', function (done) {
        axios.post('http://localhost:5000/master/group', {
            company_id: 1,
            group_name: 'GRP68',
            parent_ac_group_id: '0',
            parent_ac_group_name: 'GRP777'
        },
            {
                headers: {
                    Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImVtYWlsSWQiOiJzbXRyYWRlcnNAZ21haWwuY29tIiwiY29tcGFueV9pZCI6MX0sImlhdCI6MTY1Njk5OTMwMH0.F_MHmrM1CfySJRnCduxwGdhb-lslybj3kGHAHNr78oI'
                }
            }
        )
            .then((response, err) => {
                response.data.data.should.have.property('group_id');
                response.data.data.should.have.property('group_name');
                expect(response.status).to.equal(200);
                expect(response.data.msg).to.equal('created successfully');
                done();
            })
    })

})



describe('GET API', () => {
    it('group list', function (done) {
        axios.get('http://localhost:5000/master/group',
            {
                headers: {
                    Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImVtYWlsSWQiOiJzbXRyYWRlcnNAZ21haWwuY29tIiwiY29tcGFueV9pZCI6MX0sImlhdCI6MTY1Njk5OTMwMH0.F_MHmrM1CfySJRnCduxwGdhb-lslybj3kGHAHNr78oI'
                }
            }
        )
            .then((response, err) => {
                //    console.log(response,"response---")
                // expect(response.data.data).to.not(0);
                // expect(response.data.data.length).not(0)
                expect(response.status).to.equal(200);
                expect(response.data.message).to.equal('Listed Successfully');
                done();
            })
    })

})


describe('PUT API', () => {
    it('group update', function (done) {
        axios.put('http://localhost:5000/master/group', {
            group_id: 1,
            group_name: "GRP689",
            parent_ac_group_id: "0",
            parent_ac_group_name: "GRP777"
        },
            {
                headers: {
                    Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImVtYWlsSWQiOiJzbXRyYWRlcnNAZ21haWwuY29tIiwiY29tcGFueV9pZCI6MX0sImlhdCI6MTY1Njk5OTMwMH0.F_MHmrM1CfySJRnCduxwGdhb-lslybj3kGHAHNr78oI'
                }
            }
        )
            .then((response, err) => {
                //    console.log(response,"response---")
                // expect(response.data.data).to.not(0);
                // expect(response.data.data.length).not(0)
                // console.log(response.status)
                expect(response.status).to.equal(200);
                // expect(response.data.message).to.equal('Listed Successfully');
                done();
            })
    })

})

describe('DELETE API', () => {
    it('group delete', async () => {
        let res = await chai.request('http://localhost:5000/master')
            .delete('/group')
            .set('Authorization', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7ImVtYWlsSWQiOiJzbXRyYWRlcnNAZ21haWwuY29tIiwiY29tcGFueV9pZCI6MX0sImlhdCI6MTY1Njk5OTMwMH0.F_MHmrM1CfySJRnCduxwGdhb-lslybj3kGHAHNr78oI')
            .send({
                "group_id": 9
            })
        expect(res).to.have.status(200)
        expect(res.body.msg).to.equal('deleted successfully'); 
    })

})
