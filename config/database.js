const Sequelize = require('sequelize');

const SMERP = new Sequelize(
    'smerp',
    'pgadminkvl',
    'SMTerp@$#@!',
    {
        dialect: 'postgres',
        host: 'localhost',
        logging: false,
        port: 5432
    }
)

SMERP.authenticate()
    .then(() => {
        console.log('Connected to SMERP DB');
    })
    .catch(err => {
        console.error('Unable to connect to the SMERP DB:', err);
    });

SMERP.sync()

module.exports = { SMERP }
