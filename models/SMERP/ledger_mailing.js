const Sequelize = require('sequelize');
const {SMERP} = require('../../config/database')
const company = require('./company');
const ledger = require('./ledger');



const ledger_mailing = SMERP.define('ledger_mailing', {
    ledger_mailing_id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    ledger_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    tally_id: {
        type: Sequelize.STRING
    },
    ledger_alter_id: {
        type: Sequelize.INTEGER
    },
    ledger_name: {
        type: Sequelize.STRING
    },
   
    company_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    branch_name:{type: Sequelize.STRING},
    address1:{type: Sequelize.STRING},
    address2:{type: Sequelize.STRING},   
    address3:{type: Sequelize.STRING},
    address4:{type: Sequelize.STRING},
    state:{type: Sequelize.STRING},
    country:{type: Sequelize.STRING},
    registration_type:{type: Sequelize.STRING},
    gstin:{type: Sequelize.STRING},
    pan:{type: Sequelize.STRING},
    contact_person:{type: Sequelize.STRING},

  
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    created_by: {
        type: Sequelize.STRING
    },
    modified_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    modified_by: {
        type: Sequelize.STRING
    }
}, { freezeTableName: true,timestamps: false })

ledger_mailing.belongsTo(company, {foreignKey: {
    name: 'company_id'}
  })
  ledger_mailing.belongsTo(ledger, {foreignKey: {
    name: 'ledger_id'}
  })
module.exports = ledger_mailing;