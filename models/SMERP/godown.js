const Sequelize = require('sequelize');
const {SMERP} = require('../../config/database')
const company = require('./company');

const godown = SMERP.define('godown', {
    godown_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    tally_id: {
        type: Sequelize.STRING
    },
    godown_alter_id: {
        type: Sequelize.INTEGER
    },
    godown_name: {
        type: Sequelize.STRING
    },
    parent_godown_id: {
        type: Sequelize.INTEGER
    },
    parent_godown_name: {
        type: Sequelize.STRING
    },
    company_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    alias_name: {
        type: Sequelize.STRING
    },
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    created_by: {
        type: Sequelize.STRING
    },
    modified_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    modified_by: {
        type: Sequelize.STRING
    }
},{freezeTableName: true,timestamps: false})
godown.belongsTo(company, {foreignKey: {
    name: 'company_id'}
  })

module.exports = godown;