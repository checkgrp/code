const Sequelize = require('sequelize');
const {SMERP} = require('../../config/database')

const default_ac_group = SMERP.define('default_ac_group', {
    default_ac_group_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    tally_id: {
        type: Sequelize.STRING
    },    
    default_group_name: {
        type: Sequelize.STRING
    },
    group_alter_id:{type: Sequelize.INTEGER},
    parent_group_id:{ type: Sequelize.INTEGER},
    parent_group_name:{ type: Sequelize.STRING},
    main_group:{ type: Sequelize.STRING},
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    created_by: {
        type: Sequelize.STRING
    },
    modified_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    modified_by: {
        type: Sequelize.STRING
    }
}, { freezeTableName: true,timestamps: false })

module.exports = default_ac_group;