const Sequelize = require('sequelize');
const {SMERP} = require('../../config/database')
const company = require('./company');

const units = SMERP.define('units', {
    unit_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    tally_id: {
        type: Sequelize.STRING
    },
    unit_alter_id: {
        type: Sequelize.INTEGER
    },
    unit_symbol: {
        type: Sequelize.STRING
    },
    unit_formal_name: {
        type: Sequelize.STRING
    },
    unit_decimal_places: {
        type: Sequelize.STRING
    },
    unit_desc1: {
        type: Sequelize.STRING
    },
    unit_desc2: {
        type: Sequelize.STRING
    },
    company_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    alias_name:{
        type: Sequelize.STRING
    },
    base_unit:{type: Sequelize.DECIMAL},
    alternate_unit:{type: Sequelize.DECIMAL},
    unit:{type: Sequelize.DECIMAL},
    convertion_unit:{type: Sequelize.STRING},
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    created_by: {
        type: Sequelize.STRING
    },
    modified_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    modified_by: {
        type: Sequelize.STRING
    }
}, { freezeTableName: true,timestamps: false })
units.belongsTo(company, {foreignKey: {
    name: 'company_id'}
  })

module.exports = units;
