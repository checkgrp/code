const Sequelize = require('sequelize');
const {SMERP} = require('../../config/database')
const company = require('./company');
const stock_group = require('./stock_group');

const stock_items = SMERP.define('stock_items', {
    stock_item_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    tally_id: {
        type: Sequelize.STRING
    },
    stock_item_alter_id: {
        type: Sequelize.INTEGER
    },
    company_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    stock_item_name: {
        type: Sequelize.STRING
    },
    stock_group_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    stock_group_name: {
        type: Sequelize.STRING
    },
    base_units_id: {
        type: Sequelize.INTEGER
    },
    base_units:{
        type: Sequelize.STRING
    },
    base_unit_name: {
        type: Sequelize.STRING
    },
    alternate_base_units: {
        type: Sequelize.STRING
    },
    hsn_code: {
        type: Sequelize.STRING
    },
    taxability: {
        type: Sequelize.STRING
    },
    igst: {
        type: Sequelize.DECIMAL
    },
    cgst: {
        type: Sequelize.DECIMAL
    },
    sgst: {
        type: Sequelize.DECIMAL
    },
    quantity: {    
        type: Sequelize.DECIMAL
    },
    rate: {     
        type: Sequelize.DECIMAL
    },
    per: {        
        type: Sequelize.STRING
    },
    value: {
        type: Sequelize.DECIMAL
    },
    alias_name: {
        type: Sequelize.STRING
    },
    alternate_unit: {
        type: Sequelize.DECIMAL
    },
    taxability: {
        type: Sequelize.STRING
    },
    actual_qty:{
        type: Sequelize.DECIMAL
    },
    
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    created_by: {
        type: Sequelize.STRING
    },
    modified_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    modified_by: {
        type: Sequelize.STRING
    }
}, { freezeTableName: true,timestamps: false })

    stock_items.belongsTo(company, {foreignKey: {
        name: 'company_id'}
      })
    stock_items.belongsTo(stock_group, {foreignKey: {
        name: 'stock_group_id'}
      })

module.exports = stock_items;
