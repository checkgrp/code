const Sequelize = require('sequelize');
const {SMERP} = require('../../config/database')

const currency = SMERP.define('currency', {
    cost_centre_id: { 
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    company_id: {
        type: Sequelize.INTEGER,
    },
    tally_id: {
        type: Sequelize.STRING
    },    
    currency_alter_id: {
        type: Sequelize.INTEGER
    },
    symbol: {
        type: Sequelize.STRING
    },
    formal_name: {
        type: Sequelize.STRING
    },
    iso_currency_code: {
        type: Sequelize.STRING
    },
    no_of_decimal_places: {
        type: Sequelize.INTEGER
    },
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    created_by: {
        type: Sequelize.STRING
    },
    modified_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    modified_by: {
        type: Sequelize.STRING
    }
}, { freezeTableName: true,timestamps: false })

module.exports = currency;
