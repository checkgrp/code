const Sequelize = require('sequelize');
const {SMERP} = require('../../config/database')
const company = require('./company');
const group = require('./group');


const ledger = SMERP.define('ledger', {
    ledger_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    tally_id: {
        type: Sequelize.STRING
    },
    ledger_alter_id: {
        type: Sequelize.INTEGER
    },
    ledger_name: {
        type: Sequelize.STRING
    },
    alias_name: {
        type: Sequelize.STRING
    },
    company_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    credit_period: {
        type: Sequelize.INTEGER
    },
    mailing_name: {
        type: Sequelize.STRING
    },
    broker_name: { type: Sequelize.STRING },
    transporter_name: { type: Sequelize.STRING },
    load_man: { type: Sequelize.STRING },
    mobile_no:{type: Sequelize.STRING},
    pincode:{type: Sequelize.STRING},
    
    group_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    group_name: {
        type: Sequelize.STRING
    },
    address1: {
        type: Sequelize.STRING
    },
    address2: {
        type: Sequelize.STRING
    },
    address3: {
        type: Sequelize.STRING
    },
    address4: {
        type: Sequelize.STRING
    },
    state: {
        type: Sequelize.STRING
    },
    country: {
        type: Sequelize.STRING
    },
    
    registration_type: {
        type: Sequelize.STRING
    },
    gstin: {
        type: Sequelize.STRING
    },
    pan: {
        type: Sequelize.STRING
    },
    contact_person: {
        type: Sequelize.STRING 
    },
    opening_balance: {type: Sequelize.DECIMAL},
   
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    created_by: {
        type: Sequelize.STRING
    },
    modified_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    modified_by: {
        type: Sequelize.STRING
    }
}, { freezeTableName: true,timestamps: false })

ledger.belongsTo(company, {foreignKey: {
    name: 'company_id'}
  })
  ledger.belongsTo(group, {foreignKey: {
    name: 'group_id'}
  })
module.exports = ledger;