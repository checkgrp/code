const Sequelize = require('sequelize');
const {SMERP} = require('../../config/database')
const company = require('./company');

const cost_catagory = SMERP.define('cost_catagory', {
    cost_category_id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    tally_id: {
        type: Sequelize.STRING
    },    
    cost_category_alter_id: {
        type: Sequelize.INTEGER
    },
    cost_category_name: {
        type: Sequelize.STRING
    },
    company_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    alias_name: {
        type: Sequelize.STRING
    },
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    created_by: {
        type: Sequelize.STRING
    },
    modified_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    modified_by: {
        type: Sequelize.STRING
    }
    
},{freezeTableName: true,timestamps: false})

// cost_catagory.associate = (models) => {
//     cost_catagory.belongsTo(models.company,{foreignKey: 'company_id'});
// };
cost_catagory.belongsTo(company, {foreignKey: {
    name: 'company_id'}
  })
module.exports = cost_catagory;