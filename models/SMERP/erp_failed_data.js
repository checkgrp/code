const Sequelize = require('sequelize');
const {SMERP} = require('../../config/database')

const erp_failed_datas = SMERP.define('erp_failed_datas', {
    master: {
        type: Sequelize.STRING
    },
    operation: {
        type: Sequelize.STRING
    },
    data: {
        type: Sequelize.JSON
    },
    error: {
        type: Sequelize.JSON
    },

    
}, { freezeTableName: true,timestamps: false })
module.exports = erp_failed_datas;
