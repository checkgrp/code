const Sequelize = require('sequelize');
const {SMERP} = require('../../config/database')
const company = require('./company');
const ledger = require('./ledger');



const ledger_opening_balance = SMERP.define('ledger_opening_balance', {
    ledger_opening_balance_id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    ledger_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    tally_id: {
        type: Sequelize.STRING
    },
    ledger_alter_id: {
        type: Sequelize.INTEGER
    },
    ledger_name: {
        type: Sequelize.STRING
    },
   
    company_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    
   
    bill_date: { type: Sequelize.DATE },
    bill_no: { type: Sequelize.STRING, },
    due_date: { type: Sequelize.DATE },
    amount: { type: Sequelize.DECIMAL },
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    created_by: {
        type: Sequelize.STRING
    },
    modified_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    modified_by: {
        type: Sequelize.STRING
    }
}, { freezeTableName: true,timestamps: false })

ledger_opening_balance.belongsTo(company, {foreignKey: {
    name: 'company_id'}
  })
  ledger_opening_balance.belongsTo(ledger, {foreignKey: {
    name: 'ledger_id'}
  })
module.exports = ledger_opening_balance;