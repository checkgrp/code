const Sequelize = require('sequelize');
const {SMERP} = require('../../config/database')
const company = require('./company');

const default_voucher_type = SMERP.define('default_voucher_type', {
    default_voucher_type_id:{
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    vch_type_id: {
        type: Sequelize.INTEGER
    },
    vch_type_alt_id: {
        type: Sequelize.INTEGER
    },
    vch_type_name: { 
        type: Sequelize.STRING
    },
    vch_type_parent_id: { 
        type: Sequelize.INTEGER
    },
    parent_vch_type_name: {
        type: Sequelize.STRING
    },
    company_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    tally_id: {
        type: Sequelize.STRING
    },
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    created_by: {
        type: Sequelize.STRING
    },
    modified_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    modified_by: {
        type: Sequelize.STRING
    }
}, { freezeTableName: true, timestamps: false })


default_voucher_type.belongsTo(company, {foreignKey: {
    name: 'company_id'}
  })

module.exports = default_voucher_type;