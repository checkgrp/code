const Sequelize = require('sequelize');
const { SMERP } = require('../../config/database');
const company = require('./company');
const cost_catagory = require('./cost_catagory');

const cost_centre = SMERP.define('cost_centre', {
    cost_centre_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    tally_id: {
        type: Sequelize.STRING
    },
    cost_centre_alter_id: {
        type: Sequelize.INTEGER
    },
    company_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    cost_centre_name: {
        type: Sequelize.STRING
    },
    cost_category_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
       
    },
    cost_category_name: {
        type: Sequelize.STRING,
    },
    parent_cost_centre_name: {
        type: Sequelize.STRING
    },
    alias_name: {
        type: Sequelize.STRING
    },
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    created_by: {
        type: Sequelize.STRING
    },
    modified_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    modified_by: {
        type: Sequelize.STRING
    }
}, { freezeTableName: true,timestamps: false })

    cost_centre.belongsTo(company, {foreignKey: {
        name: 'company_id'}
      })
    
    cost_centre.belongsTo(cost_catagory, {foreignKey: {
        name: 'cost_category_id'}
      })

module.exports = cost_centre;