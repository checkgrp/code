const Sequelize = require('sequelize');
const {SMERP} = require('../../config/database')
const company = require('./company');
// const default_voucher_type = require('./default_voucher_type')

const voucher_type = SMERP.define('voucher_type', {
    voucher_type_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    tally_id: {
        type: Sequelize.STRING
    },
    voucher_type_alter_id: {
        type: Sequelize.INTEGER
    },
    voucher_name: {
        type: Sequelize.STRING
    },
     voucher_type: {
        type: Sequelize.STRING
    },
    company_id: {
        type: Sequelize.INTEGER,
        foreignKey:true,
    },
    company_name: {
        type: Sequelize.STRING
    },
    production_journal: {
        type: Sequelize.STRING
    },
    alias_name: {
        type: Sequelize.STRING
    },
    vtyp_cmp_name:{type: Sequelize.STRING},
    vtyp_cmp_mobile:{type: Sequelize.STRING},
    groups_exclude: {
        type: Sequelize.STRING
    },
    groups_include: {
        type: Sequelize.STRING
    },
    activate: {
        type: Sequelize.STRING
    },
    abbrevation: {
        type: Sequelize.STRING 
    },
    voucher_numbering: {
        type: Sequelize.STRING
    },
    allow_narration: {
        type: Sequelize.STRING
    },
    allow_transaactions: {
        type: Sequelize.STRING
    },
    provide_narration: {
        type: Sequelize.STRING 
    },
    
    address1: {
        type: Sequelize.STRING
    },
    address2: {
        type: Sequelize.STRING
    },
    address3: {
        type: Sequelize.STRING
    },
  
    
    phone:{
        type: Sequelize.STRING
    },
    fssai_no:{
        type: Sequelize.STRING
    },
    bank_name:{
        type: Sequelize.STRING
    },
    company_tin:{
        type: Sequelize.STRING
    },
    company_gstin:{
        type: Sequelize.STRING
    },
    company_cst:{
        type: Sequelize.STRING
    },
    v_type:{
        type: Sequelize.STRING
    },
    pos_invoicing:{
        type: Sequelize.STRING
    },
    default_title:{
        type: Sequelize.STRING
    },
    default_bank:{
        type: Sequelize.STRING
    },
    default_jurisdiction:{
        type: Sequelize.STRING
    },
    tax_invoice:{
        type: Sequelize.STRING
    },
    set_declartion:{
        type: Sequelize.STRING
    },
    print_voucher: {
        type: Sequelize.STRING
    },
    advance_config: {
        type: Sequelize.STRING
    },
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    created_by: {
        type: Sequelize.STRING
    },
    modified_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    modified_by: {
        type: Sequelize.STRING
    }
}, { freezeTableName: true,timestamps: false })

voucher_type.belongsTo(company, {foreignKey: {
    name: 'company_id'}
  })
  
  
//   voucher_type.belongsTo(default_voucher_type, {foreignKey: {
//     name: 'vch_type_name'}
//   })
module.exports = voucher_type;