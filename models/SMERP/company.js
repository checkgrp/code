const Sequelize = require('sequelize');
const {SMERP} = require('../../config/database')

const company = SMERP.define('company', {
    company_id: {
        type: Sequelize.INTEGER, 
        primaryKey: true,
        autoIncrement: true
    },
    tally_id: {
        type: Sequelize.STRING
    },
    ledger_id: {
        type: Sequelize.INTEGER
    },
    company_alter_id: {
        type: Sequelize.INTEGER
    },
    company_name: {
        type: Sequelize.STRING
    },
    company_master_id:{
        type: Sequelize.INTEGER
    },
    alias_name:{
        type: Sequelize.STRING
    },
    address: {
        type: Sequelize.STRING
    },
    address1: {
        type: Sequelize.STRING
    },
    
    address2: {
        type: Sequelize.STRING
    },
    pincode: {
        type: Sequelize.STRING
    },
    state: {
        type: Sequelize.STRING
    },
    phone_no: {
        type: Sequelize.STRING
    },
    mobile_no: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    fssai_no: {
        type: Sequelize.STRING
    },
    bank_name: {
        type: Sequelize.STRING
    },
    tin: {
        type: Sequelize.STRING
    },
    gstin: {
        type: Sequelize.STRING
    },
    fin_year_start: {
        type: Sequelize.DATE
    },
    fin_year_end: {
        type: Sequelize.DATE
    },
    base_currency_symbol: {
        type: Sequelize.STRING
    },
    formal_name: {
        type: Sequelize.STRING
    },
    is_deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    created_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    created_by: {
        type: Sequelize.STRING
    },
    modified_on: {
        type: 'TIMESTAMP',
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    modified_by: {
        type: Sequelize.STRING
    }
}, { freezeTableName: true,timestamps: false })
module.exports = company;
