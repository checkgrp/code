const helper = require("../utils/helper");
require('dotenv').config()
var jwt = require('jsonwebtoken');

const authController = () =>{
const login = async (req,res) =>{
    try{
      const userInput = helper.getReqValues(req)

      // if(userInput.emailId != process.env.emailId){
      //   res.status(500).json({ 
      //     status:500,
      //     message:'Incorrect email ID'
      // })
      // }

          // if(userInput.emailId == process.env.emailId){
            //   if(userInput.password != process.env.password){
            //     res.status(500).json({ 
            //         status:500,
            //         message:'Incorrect password'
            //     })
            // }
            // else{
              res.json({
                status: 201,
                message : 'login successful',
                data : {
                    emailId: userInput.emailId
                },
                token: jwt.sign({data: { emailId: userInput.emailId, company_id : 1 }},
                  process.env.secretKey, {
                    expiresIn: "72h"}
                 )
                })
            // }
              // }
  
        
    }catch (err) {
      console.log("Error in login :", err);
      res.json({
        status: false,
        message: "Internal server Error",
      });
    }
}

return{
    login
}

}

module.exports = authController();
