const company = require('../models/SMERP/company')
const cost_centre = require('../models/SMERP/cost_centre')
const cost_catagory = require('../models/SMERP/cost_catagory')
const currency = require('../models/SMERP/currency')
const default_ac_group = require('../models/SMERP/default_ac_group')
const godown = require('../models/SMERP/godown')
const group = require('../models/SMERP/group')
const ledger = require('../models/SMERP/ledger')
const ledger_opening_balance = require('../models/SMERP/ledger_opening_balance')
const ledger_mailing = require('../models/SMERP/ledger_mailing')
const opening_balance = require('../models/SMERP/opening_balance')
const stock_group = require('../models/SMERP/stock_group')
const stock_items = require('../models/SMERP/stock_items')
const units = require('../models/SMERP/units')
const voucher_type = require('../models/SMERP/voucher_type')
const default_voucher_type = require('../models/SMERP/default_voucher_type')

const erp_failed_datas = require('../models/SMERP/erp_failed_data')





const logger = require('../initlog')
const helper = require('../utils/helper')
const commoncontoller = require('../utils/common')
const sendToQueue = require('../messageQueue/sender')


const DBController = () => {
    const create = async (req, res) => {
        try {
            const module = commoncontoller.GetLastURL(req.originalUrl)
            console.log(module)
            const modal = commoncontoller.CheckIfModuleIsPresentOrNot(module)
            console.log(modal)
            const userInput = helper.getReqValues(req)
            console.log(userInput, "userinput----")
            await modal.create(userInput).then(async datas => {
                if (userInput?.record_status) {
                    // sendToQueue(module, JSON.stringify({
                    //     record_status: 'success',
                    //     tally_id: userInput.tally_id,
                    //     table: await commoncontoller.GetLastURL(req.originalUrl),
                    //     operation: 'INSERT'
                    // }))
                    // logger.info(JSON.stringify({
                    //     status: 200,
                    //     method: "POST",
                    //     master: modal,
                    //     message: 'created successfully',
                    //     origin: "Tally Outbound"
                    // }));
                    res.json({
                        status: 200,
                        method: "POST",
                        master: modal,
                        msg: "created successfully",
                    })
                } else {
                    datas.dataValues.operation = 'INSERT'
                    datas.dataValues.table = commoncontoller.GetLastURL(req.originalUrl)
                    console.log(datas.dataValues, "datas====")
                    // sendToQueue(module, JSON.stringify(datas.dataValues))
                    res.status(200).json({
                        msg: "created successfully",
                        data: datas.dataValues
                    })
                    // logger.info(JSON.stringify({
                    //     status: 200,
                    //     method: "POST",
                    //     master: modal,
                    //     message: 'created successfully',
                    //     origin: "ERP"
                    // }));
                }

            }).catch(async err => {
                if (userInput?.record_status) {
                    // sendToQueue(module, JSON.stringify({
                    //     record_status: 'failed',
                    //     tally_id: userInput.tally_id,
                    //     table: await commoncontoller.GetLastURL(req.originalUrl),
                    //     operation: 'INSERT'

                    // }))
                    const failedDatas = {
                        master: module,
                        operation: 'INSERT',
                        data: userInput,
                        error: err
                    }
                    const updateData = await erp_failed_datas.create(failedDatas)

                    // logger.error(JSON.stringify({
                    //     status: 500,
                    //     method: "POST",
                    //     master: modal,
                    //     message: 'failed',
                    //     origin: "Tally Outbound",
                    //     data: userInput,
                    //     error: err
                    // }));
                    res.json({
                        status: 500,
                        method: "POST",
                        master: modal,
                        data: userInput,
                        error: err,
                        msg: "failed",
                    })
                } else {
                    res.status(500).json({
                        msg: "error occured",
                        error: err
                    })

                    // logger.error(JSON.stringify({
                    //     status: 500,
                    //     method: "POST",
                    //     master: modal,
                    //     message: 'failed',
                    //     origin: "ERP",
                    //     data: userInput,
                    //     error: err
                    // }));

                }

            })
        } catch (err) {
            console.log("internal server error", err)
            res.json({
                error: err,
                response: res
            })
        }
    }


    const read = async (req, res) => {
        try {
            const userInput = helper.getReqValues(req);
            const module = commoncontoller.GetLastURL(req.originalUrl)
            const modal = commoncontoller.CheckIfModuleIsPresentOrNot(module)
            //  console.log(userInput)
            await modal.findAll(
                { where: { is_deleted: false } }
            )
                .then((data) => {
                    if (data) {
                        res.json({
                            data: data, message: "Listed Successfully"
                        });
                    }
                });
        } catch (err) {
            console.log("Error in list :", err);
            res.json({
                status: false,
                message: "Internal server Error",
            });
        }
    }

    const update = async (req, res) => {
        try {
            const module = commoncontoller.GetLastURL(req.originalUrl)
            const modal = commoncontoller.CheckIfModuleIsPresentOrNot(module)
            const userInput = helper.getReqValues(req)

            console.log("successupdate----")
            let condn = {}
            let others = {}
            if (userInput?.data?.record_status) {
                condn[userInput.uniqueKey] = userInput.id
                others = userInput.data
            }
            else {
                condn[Object.keys(userInput)[0]] = Object.values(userInput)[0]
                console.log(Object.keys(userInput)[0], "keyss---")
                others = userInput
            }
            console.log(condn, "condn-----")
            console.log(userInput, "userInput-----")

            await modal.update(others, {
                where: condn
            }).then(async datas => {
                if (datas[0] == 0) {
                    res.status(200).json({
                        msg: "No such data found",
                    })
                    logger.error(JSON.stringify({
                        status: 404,
                        method: "PUT",
                        master: module,
                        message: 'Not Found',
                        data: userInput,
                    }));
                } else {
                    if (userInput?.data?.record_status) {

                        // sendToQueue(module, JSON.stringify({
                        //     [userInput.uniqueKey]: userInput.id,
                        //     record_status: 'success',
                        //     table: await commoncontoller.GetLastURL(req.originalUrl),
                        //     operation: 'UPDATE'
                        // }))

                        // logger.info(JSON.stringify({
                        //     status: 200,
                        //     method: "PUT",
                        //     master: module,
                        //     message: 'updated successfully',
                        //     origin: "Tally Outbound"
                        // }));

                        res.json({
                            status: 200,
                            method: "PUT",
                            master: modal,
                            data: userInput,
                            msg: "updated successfully",
                        })

                    } else {
                        const findData = await modal.findOne({ where: condn })
                        findData.dataValues.operation = 'UPDATE',
                            findData.dataValues.table = commoncontoller.GetLastURL(req.originalUrl)
                        // sendToQueue(module, JSON.stringify(findData.dataValues))
                        res.status(200).json({
                            msg: "updated successfully",
                            data: userInput
                        })

                        // logger.info(JSON.stringify({
                        //     status: 200,
                        //     method: "PUT",
                        //     master: module,
                        //     message: 'updated successfully',
                        //     origin: "ERP"
                        // }));
                    }
                }
            }).catch(async err => {

                if (userInput?.data?.record_status) {

                    // sendToQueue(module, JSON.stringify({
                    //     [userInput.uniqueKey]: userInput.id,
                    //     record_status: 'failed',
                    //     table: await commoncontoller.GetLastURL(req.originalUrl),
                    //     operation: 'UPDATE'
                    // }))


                    const failedDatas = {
                        master: module,
                        operation: 'UPDATE',
                        data: userInput,
                        error: err
                    }

                    const updateData = await erp_failed_datas.create(failedDatas)

                    // logger.error(JSON.stringify({
                    //     status: 500,
                    //     method: "PUT",
                    //     master: module,
                    //     message: 'failed',
                    //     origin: "Tally Outbound",
                    //     data: userInput,
                    //     error: err
                    // }));

                    res.json({
                        status: 500,
                        method: "PUT",
                        master: modal,
                        data: userInput,
                        error: err,
                        msg: "failed",
                    })

                } else {
                    res.status(500).json({
                        msg: "error occured",
                        error: err
                    })

                    // logger.error(JSON.stringify({
                    //     status: 500,
                    //     method: "PUT",
                    //     master: module,
                    //     message: 'failed',
                    //     origin: "ERP",
                    //     data: userInput,
                    //     error: err
                    // }));
                }

            })
        } catch (err) {
            res.json({
                error: err,
                response: res
            })
        }
    }

    const Delete = async (req, res) => {
        try {
            const module = commoncontoller.GetLastURL(req.originalUrl);
            const modal = commoncontoller.CheckIfModuleIsPresentOrNot(module)
            const userInput = helper.getReqValues(req)
            let condn = {}
            if (userInput?.data?.record_status) {
                condn[userInput.uniqueKey] = userInput.id
            }
            else {
                condn[Object.keys(userInput)[0]] = Object.values(userInput)[0]
            }

            await modal.update({ is_deleted: true }, {
                where: condn
            }).then(async datas => {
                if (datas === 0) {
                    res.status(200).json({
                        msg: "No such data found",
                    })
                    // logger.error(JSON.stringify({
                    //     status: 404,
                    //     method: "DELETE",
                    //     master: module,
                    //     message: 'Not Found',
                    //     data: userInput,
                    // }));
                } else {
                    if (!userInput?.data?.record_status) {
                        userInput.operation = 'DELETE',
                            userInput.table = commoncontoller.GetLastURL(req.originalUrl)
                        // sendToQueue(module, JSON.stringify(userInput))
                        res.status(200).json({
                            msg: "deleted successfully",
                        })
                        // logger.info(JSON.stringify({
                        //     status: 200,
                        //     method: "DELETE",
                        //     master: modal,
                        //     message: 'deleted successfully',
                        //     origin: "ERP"
                        // }));
                    } else {
                        console.log("Record Deleted Successfully")

                        // sendToQueue(module, JSON.stringify({
                        //     [userInput.uniqueKey]: userInput.id,
                        //     record_status: 'success',
                        //     table: await commoncontoller.GetLastURL(req.originalUrl),
                        //     operation: 'DELETE'
                        // }))

                        // logger.info(JSON.stringify({
                        //     status: 200,
                        //     method: "DELETE",
                        //     master: modal,
                        //     message: 'deleted successfully',
                        //     origin: "Tally Outbound"
                        // }));

                        res.json({
                            status: 200,
                            method: "DELETE",
                            master: modal,
                            data: userInput,
                            msg: "deleted successfully",
                        })

                    }
                }


            }).catch(async err => {
                if (!userInput?.data?.record_status) {
                    res.status(500).json({
                        msg: "error occured",
                        error: err
                    })

                    // logger.error(JSON.stringify({
                    //     status: 500,
                    //     method: "DELETE",
                    //     master: modal,
                    //     message: 'failed',
                    //     origin: "ERP",
                    //     data: userInput,
                    //     error: err
                    // }));
                } else {
                    console.log("No Record Found")
                    const failedDatas = {
                        master: module,
                        operation: 'DELETE',
                        data: userInput,
                        error: err
                    }

                    const updateData = await erp_failed_datas.create(failedDatas)
                    // logger.error(JSON.stringify({
                    //     status: 500,
                    //     method: "DELETE",
                    //     master: modal,
                    //     message: 'failed',
                    //     origin: "Tally Outbound",
                    //     data: userInput,
                    //     error: err
                    // }));

                    res.json({
                        status: 500,
                        method: "DELETE",
                        master: modal,
                        data: userInput,
                        error: err,
                        msg: "deleted successfully",
                    })
                }

            })
        } catch (err) {
            res.json({
                error: err,
                response: res
            })
        }
    }




    const multipleUpdate = async (req, res) => {
        try {
            const module = commoncontoller.GetUpdteURL(req.originalUrl)
            const modal = commoncontoller.CheckIfModuleIsPresentOrNot(module)
            const userInput = helper.getReqValues(req)
            console.log(modal, "module-----")
            let condn = {}
            let others = {}
            let condn2 = {}
            let others2 = {}

            if (module == 'stock_group') {
                condn.stock_group_id = userInput.id
                others.stock_group_name = userInput.name

                condn2.parent_stock_group_id = userInput.id
                others2.parent_stock_group_name = userInput.name
            }
            if (module == 'group') {
                condn.group_id = userInput.id
                others.group_name = userInput.name

                condn2.parent_ac_group_id = userInput.id
                others2.parent_ac_group_name = userInput.name
            }
            if (module == 'godown') {
                condn.godown_id = userInput.id
                others.godown_name = userInput.name

                condn2.parent_godown_id = userInput.id
                others2.parent_godown_name = userInput.name
            }
            console.log(condn, "condn-----")
            console.log(userInput, "userInput-----")

            await modal.update(others, {
                where: condn
            }
            ).then(async datas => {
                console.log(datas, "datas----")

                if (datas[0] == 0) {
                    res.status(200).json({
                        msg: "No such data found",
                    })
                    // logger.error(JSON.stringify({
                    //     status: 404,
                    //     method: "PUT",
                    //     master: module,
                    //     message: 'Not Found',
                    //     data: userInput,
                    // }));
                } else {
                    const updatedData = await modal.update(others2, {
                        where: condn2
                    })
                    console.log(updatedData, "updatedData----")
                    if (updatedData) {
                        res.status(200).json({
                            msg: "updated successfully",
                            data: userInput
                        })

                        // logger.info(JSON.stringify({
                        //     status: 200,
                        //     method: "PUT",
                        //     master: modal,
                        //     message: 'updated successfully',
                        // }));
                    }

                }
            }).catch(async err => {

                res.status(500).json({
                    msg: "error occured",
                    error: err
                })

                // logger.error(JSON.stringify({
                //     status: 500,
                //     method: "PUT",
                //     master: modal,
                //     message: 'failed',
                //     origin: "ERP",
                //     data: userInput,
                //     error: err
                // }));


            })
        } catch (err) {
            res.json({
                error: err,
                response: res
            })
        }
    }

    return {
        create,
        read,
        update,
        Delete,
        multipleUpdate
    }
}

module.exports = DBController()
