const winston = require('winston');
var now = new Date();

const logConfiguration = {
    'transports': [
        new winston.transports.File({
            filename: './logs/erp_server' + now.getFullYear() + "-"+ now.getMonth() + "-" + now.getDate() +'.log'
        })
    ],
    format: winston.format.combine(
        winston.format.label({
            label: `erp_server`
        }),
        winston.format.timestamp({
           format: 'MMM-DD-YYYY HH:mm:ss'
       }),
        winston.format.printf(info => `${info.level}: ${info.label}: ${[info.timestamp]}: ${info.message}`),
    )
};
const logger = winston.createLogger(logConfiguration);
module.exports= logger;