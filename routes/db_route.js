var express = require('express');
var router = express.Router();
const DBController = require('../controller/dbcontroller')

router.post('/', DBController.create) //create
router.get('/', DBController.read) //read
router.put('/', DBController.update) //update
router.delete('/', DBController.Delete) //delete

router.put('/multipleUpdate', DBController.multipleUpdate)//multipleupdate


module.exports = router;