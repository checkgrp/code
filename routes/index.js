var express = require('express');
var router = express.Router();
const DBRoute = require('./db_route')
const authController = require('../controller/authcontroller')

router.post('/login', authController.login)

router.use('/:module', DBRoute)



module.exports = router;


